# silver-bat-01-seeking-refuge

Source files for an open novel

The final version of the e-books can be found [on the Internet Archive](https://archive.org/details/seekingrefuge/page/n15).

I would give some background on the novel, here, but have already done that in the book itself in [the Credits](text/20-credits.md).  There, you will find the inspirations and sources for pretty much everything in the story.

If you see anything that doesn't work, feel free to submit a pull request or file an issue, keeping in mind that I'll obviously maintain final editorial control.
